const express = require('express')
const app = express()
const port = 4000

app.get('/', (request, response) => {
    response.send('Eleazar Jared Lopez Osuna - 201700893')
})

app.listen(port, () => {
  console.log(`Servidor corriendo en el puerto ${port}`)
})