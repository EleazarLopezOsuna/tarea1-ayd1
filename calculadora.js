const express = require('express')
const bodyParser = require("body-parser");
const app = express()
const port = 3000

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

app.post('/suma', (request, response) => {
    const numero1 = parseInt(request.body.numero1)
    const numero2 = parseInt(request.body.numero2)
    const suma = numero1 + numero2
    response.send('El resultado es ' + suma)
})

app.post('/resta', (request, response) => {
  const numero1 = parseInt(request.body.numero1)
  const numero2 = parseInt(request.body.numero2)
  const resta = numero1 - numero2
  response.send('El resultado es ' + resta)
})

app.post('/multiplicacion', (request, response) => {
  const numero1 = parseInt(request.body.numero1)
  const numero2 = parseInt(request.body.numero2)
  const multiplicacion = numero1 * numero2
  response.send('El resultado es ' + multiplicacion)
})

app.listen(port, () => {
  console.log(`Servidor corriendo en el puerto ${port}`)
})